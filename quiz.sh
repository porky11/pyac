#!/bin/sh

rm ./*.c
rm ./*.h
rm ./*.cl
sh autogen.sh
make clean
sh configure
make -j8
valgrind -q binary/spal_dictionary probe/pyashWords.txt probe/pyashWords.h
valgrind --track-origins=yes --leak-check=full -q binary/spal probe/trop.pyac
#strace binary/spal probe/trop.pya
#valgrind -q binary/spal_translation probe/en.kwon probe/trop.byin
