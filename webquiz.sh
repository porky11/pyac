#!/bin/sh
EMROOT="$HOME/Downloads/emsdk/emscripten/1.37.14/"
rm ./*.c
rm ./*.h
rm ./*.cl
sh autogen.sh
make clean
"$EMROOT/emconfigure" ./configure
"$EMROOT/emmake" make -j8
"$EMROOT/emcc" library/library_encoding.a -s \
  EXPORTED_FUNCTIONS="['_text_encoding_quiz']" -o encoding.js

#valgrind -q binary/spal_dictionary probe/pyashWords.txt probe/pyashWords.h
#valgrind --track-origins=yes --leak-check=full -q binary/spal probe/trop.pyac
#strace binary/spal probe/trop.pya
#valgrind -q binary/spal_translation probe/en.kwon probe/trop.byin
