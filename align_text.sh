#!/bin/bash
FILES="program/parallel/composition_population.cl program/dictionary.c \
      program/seed.c  program/opencl_programmer.c program/seed.h program/spal.c \
      program/spal_compile.c program/spal_translation.c program/dialogue.c \
      program/dictionary.h program/genericOpenCL.c program/genericOpenCL.h program/spal.h \
      program/hello.c program/encoding.c program/clprobe.c program/spal_dictionary.c\
      program/parallel/quiz_population.cl program/translation.c \
      program/translation.h program/pyash.c program/sort.h program/sort.c"
  
for i in $FILES
do
  clang-format "$i" > tmp;
  mv tmp "$i";
done
