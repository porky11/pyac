#include "pyash.h"
#include "pyashWords.h"
#include <stdint.h>
uint16_t grammaticalCase_word_series[] = {nominative_case_GRAMMAR,
                                          accusative_case_GRAMMAR,
                                          topic_case_GRAMMAR,
                                          vocative_case_GRAMMAR,
                                          genitive_case_GRAMMAR,
                                          destination_case_GRAMMAR,
                                          dative_case_GRAMMAR,
                                          instrumental_case_GRAMMAR,
                                          comitative_case_GRAMMAR,
                                          benefactive_case_GRAMMAR,
                                          essive_case_GRAMMAR,
                                          adpositional_case_GRAMMAR,
                                          location_case_GRAMMAR,
                                          way_case_GRAMMAR,
                                          source_case_GRAMMAR,
                                          ablative_case_GRAMMAR,
                                          elative_case_GRAMMAR,
                                          illative_case_GRAMMAR,
                                          temporal_case_GRAMMAR,
                                          perlative_case_GRAMMAR,
                                          delative_case_GRAMMAR,
                                          causal_case_GRAMMAR,
                                          terminative_case_GRAMMAR,
                                          inessive_case_GRAMMAR,
                                          allative_case_GRAMMAR,
                                          vialis_case_GRAMMAR,
                                          quotative_case_GRAMMAR,
                                          distributive_case_GRAMMAR,
                                          evidential_case_GRAMMAR,
                                          multiplicative_case_GRAMMAR,
                                          initiative_case_GRAMMAR,
                                          sublative_case_GRAMMAR,
                                          exessive_case_GRAMMAR,
                                          pegative_case_GRAMMAR,
                                          prosecutive_case_GRAMMAR,
                                          superessive_case_GRAMMAR,
                                          partitive_case_GRAMMAR,
                                          prolative_case_GRAMMAR,
                                          modal_case_GRAMMAR,
                                          centric_case_GRAMMAR,
                                          oblique_case_GRAMMAR,
                                          patient_case_GRAMMAR,
                                          lative_case_GRAMMAR,
                                          locative_case_GRAMMAR,
                                          abessive_case_GRAMMAR,
                                          adessive_case_GRAMMAR,
                                          agentive_case_GRAMMAR,
                                          ergative_case_GRAMMAR,
                                          evitative_case_GRAMMAR,
                                          subessive_case_GRAMMAR,
                                          possessed_case_GRAMMAR,
                                          antessive_case_GRAMMAR,
                                          adverbial_case_GRAMMAR,
                                          exocentric_case_GRAMMAR,
                                          absolutive_case_GRAMMAR,
                                          associative_case_GRAMMAR,
                                          superlative_case_GRAMMAR,
                                          postpositional_case_GRAMMAR,
                                          locative_directional_case_GRAMMAR};

uint16_t perspective_word_series[] = {realis_mood_GRAMMAR,
                                      deontic_mood_GRAMMAR,
                                      conditional_mood_GRAMMAR,
                                      epistemic_mood_GRAMMAR,
                                      interrogative_mood_GRAMMAR,
                                      directive_mood_GRAMMAR,
                                      hortative_mood_GRAMMAR,
                                      volitive_mood_GRAMMAR,
                                      deliberative_mood_GRAMMAR,
                                      desiderative_mood_GRAMMAR,
                                      imperative_mood_GRAMMAR,
                                      optative_mood_GRAMMAR,
                                      potential_mood_GRAMMAR,
                                      dubitative_mood_GRAMMAR,
                                      precative_mood_GRAMMAR,
                                      jussive_mood_GRAMMAR,
                                      permissive_mood_GRAMMAR,
                                      commissive_mood_GRAMMAR,
                                      eventive_mood_GRAMMAR,
                                      speculative_mood_GRAMMAR,
                                      benedictive_mood_GRAMMAR,
                                      inductive_mood_GRAMMAR,
                                      admonitive_mood_GRAMMAR,
                                      apprehensive_mood_GRAMMAR,
                                      imprecative_mood_GRAMMAR,
                                      assumptive_mood_GRAMMAR,
                                      prohibitive_mood_GRAMMAR,
                                      declarative_mood_GRAMMAR,
                                      affirmative_mood_GRAMMAR,
                                      irrealis_mood_GRAMMAR,
                                      sensory_evidential_mood_GRAMMAR,
                                      gnomic_mood_GRAMMAR,
                                      propositive_mood_GRAMMAR,
                                      necessitative_mood_GRAMMAR};

uint16_t number_word_series[] = {zero_WORD,
  one_WORD,
  two_WORD,
  three_WORD, four_WORD, five_WORD, six_WORD, seven_WORD, eight_WORD, nine_WORD, ten_WORD, eleven_WORD, twelve_WORD, thirteen_WORD,
  fourteen_WORD, fifteen_WORD, sixteen_WORD, seventeen_WORD, eighteen_WORD, nineteen_WORD, twenty_WORD,
};
uint8_t number_word_series_long = sizeof(number_word_series)/sizeof(number_word_series[0]);

uint16_t word_belongs_INT(const uint16_t word, const uint16_t series_long, uint16_t *series) {
  uint16_t indexFinger = 0;
  for (; indexFinger < series_long; ++indexFinger) {
    if (series[indexFinger] == word) {
      return truth_WORD;
    }
  }
  return lie_WORD;
}

uint16_t number_word_INT(const uint16_t word) { 
  return word_belongs_INT(word, number_word_series_long, number_word_series);
}
