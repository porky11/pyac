#include "sort.h"
#include "pyash.h"
#include <assert.h>
#include <stdio.h>

void v4us_write(uint8_t code_indexFinger, uint8_t maximum_code_indexFinger,
                uint16_t code_number, v4us *code_name) {
  if (code_indexFinger >= maximum_code_indexFinger)
    return;
  else if (code_indexFinger == 0) {
    (*code_name).s0 = code_number;
  } else if (code_indexFinger == 1) {
    (*code_name).s1 = code_number;
  } else if (code_indexFinger == 2)
    (*code_name).s2 = code_number;
}

uint64_t v4us_uint64_translation(const v4us vector) {
  return (uint64_t)(
      ((uint64_t)vector.s0 << (16 * 0)) + ((uint64_t)vector.s1 << (16 * 1)) +
      ((uint64_t)vector.s2 << (16 * 2)) + ((uint64_t)vector.s3 << (16 * 3)));
}

void v16us_write(const uint8_t indexFinger, const uint16_t number,
                 v16us *vector) {
  assert(indexFinger < TABLET_LONG);
  assert(vector != NULL);
  switch (indexFinger) {
  case 0:
    (*vector).s0 = number;
    break;
  case 1:
    (*vector).s1 = number;
    break;
  case 2:
    (*vector).s2 = number;
    break;
  case 3:
    (*vector).s3 = number;
    break;
  case 4:
    (*vector).s4 = number;
    break;
  case 5:
    (*vector).s5 = number;
    break;
  case 6:
    (*vector).s6 = number;
    break;
  case 7:
    (*vector).s7 = number;
    break;
  case 8:
    (*vector).s8 = number;
    break;
  case 9:
    (*vector).s9 = number;
    break;
  case 0xA:
    (*vector).sA = number;
    break;
  case 0xB:
    (*vector).sB = number;
    break;
  case 0xC:
    (*vector).sC = number;
    break;
  case 0xD:
    (*vector).sD = number;
    break;
  case 0xE:
    (*vector).sE = number;
    break;
  case 0xF:
    (*vector).sF = number;
    break;
  }
}
uint16_t v16us_read(const uint8_t indexFinger, const v16us vector) {
  assert(indexFinger < TABLET_LONG);
  switch (indexFinger) {
  case 0:
    return (uint16_t)vector.s0;
    break;
  case 1:
    return (uint16_t)vector.s1;
    break;
  case 2:
    return (uint16_t)vector.s2;
    break;
  case 3:
    return (uint16_t)vector.s3;
    break;
  case 4:
    return (uint16_t)vector.s4;
    break;
  case 5:
    return (uint16_t)vector.s5;
    break;
  case 6:
    return (uint16_t)vector.s6;
    break;
  case 7:
    return (uint16_t)vector.s7;
    break;
  case 8:
    return (uint16_t)vector.s8;
    break;
  case 9:
    return (uint16_t)vector.s9;
    break;
  case 0xA:
    return (uint16_t)vector.sA;
    break;
  case 0xB:
    return (uint16_t)vector.sB;
    break;
  case 0xC:
    return (uint16_t)vector.sC;
    break;
  case 0xD:
    return (uint16_t)vector.sD;
    break;
  case 0xE:
    return (uint16_t)vector.sE;
    break;
  case 0xF:
    return (uint16_t)vector.sF;
    break;
  default:
    assert(1 == 0); //"invalid value"
  }
}

int v16us_print(const v16us tablet) {
  return printf("0x(%04X %04X %04X %04X  %04X %04X %04X %04X  %04X %04X %04X "
                "%04X  %04X %04X %04X %04X)\n",
                tablet.s0, tablet.s1, tablet.s2, tablet.s3, tablet.s4,
                tablet.s5, tablet.s6, tablet.s7, tablet.s8, tablet.s9,
                tablet.sA, tablet.sB, tablet.sC, tablet.sD, tablet.sE,
                tablet.sF);
}
int tablet_print(const uint8_t series_long, const v16us *tablet) {
  int print_gross = 0;
  uint8_t indexFinger = 0;
  for (; indexFinger < series_long; ++indexFinger) {
    print_gross += printf("\t");
    print_gross += v16us_print(tablet[indexFinger]);
  }
  return print_gross;
}

void v16us_forward_copy(const uint8_t abl, const uint8_t dat, const uint8_t ins,
                        v16us *tablet) {
  assert(dat + ins <= 0xF);
  assert(abl < dat);
  assert(tablet != NULL);
  uint8_t indexFinger = dat + ins;
  for (; indexFinger >= abl; --indexFinger) {
    v16us_write(indexFinger, v16us_read(indexFinger - ins, *tablet), tablet);
  }
}

uint16_t tablet_read(const uint16_t indexFinger, const uint8_t series_long,
                     const v16us *tablet) {
  assert(TABLET_LONG * series_long > indexFinger);
  assert(tablet != NULL);
  return v16us_read(indexFinger % TABLET_LONG,
                    tablet[indexFinger / TABLET_LONG]);
}
void tablet_write(const uint16_t indexFinger, const uint16_t number,
                  const uint8_t series_long, v16us *tablet) {
  assert(TABLET_LONG * series_long > indexFinger);
  assert(tablet != NULL);
  v16us_write(indexFinger % TABLET_LONG, number,
              tablet + indexFinger / TABLET_LONG);
}

void tablet_word_write(const uint16_t indexFinger, const uint16_t number,
                       const uint8_t series_long, v16us *tablet,
                       uint16_t *neo_indexFinger) {
  assert(TABLET_LONG * series_long > indexFinger);
  assert(tablet != NULL);
  assert(neo_indexFinger != NULL);
  *neo_indexFinger = indexFinger;
  uint16_t list_indexFinger = indexFinger - (indexFinger % TABLET_LONG);
  uint16_t binary_phrase_list =
      tablet_read(list_indexFinger, series_long, tablet);
  if (binary_phrase_list == 0) {
    binary_phrase_list = 1;
    tablet_write(list_indexFinger, binary_phrase_list, series_long, tablet);
  }
  uint8_t vector_indexFinger = indexFinger % TABLET_LONG;
  uint8_t series_indexFinger = 0;
  uint8_t indicator = 0;
  if (vector_indexFinger == 0) {
    ++*neo_indexFinger;
    // also invert the previous binary_phrase_list, if it exists.
    series_indexFinger = (uint8_t)indexFinger / TABLET_LONG;
    if (series_indexFinger > 0) {
      binary_phrase_list = (uint16_t)tablet[series_indexFinger - 1].s0;
      indicator = binary_phrase_list & 1;
      if (indicator == TABLET_FINALLY_INDICATOR) {
        binary_phrase_list = ~binary_phrase_list;
        tablet[series_indexFinger - 1].s0 = binary_phrase_list;
      }
    }
  }
  tablet_write(*neo_indexFinger, number, series_long, tablet);
}
void tablet_retrospective_word_write(const uint16_t indexFinger,
                                     const uint16_t number,
                                     const uint8_t series_long, v16us *tablet,
                                     uint16_t *neo_indexFinger) {
  assert(TABLET_LONG * series_long > indexFinger);
  assert(tablet != NULL);
  assert(neo_indexFinger != NULL);
  *neo_indexFinger = indexFinger;
  if (indexFinger % TABLET_LONG == 0) {
    --*neo_indexFinger;
  }
  tablet_write(*neo_indexFinger, number, series_long, tablet);
}
uint16_t tablet_upcoming_word_read(const uint16_t indexFinger,
                                   const uint8_t series_long,
                                   const v16us *tablet,
                                   uint16_t *neo_indexFinger) {
  assert(TABLET_LONG * series_long > indexFinger);
  assert(tablet != NULL);
  assert(neo_indexFinger != NULL);
  *neo_indexFinger = indexFinger;
  if (indexFinger % TABLET_LONG == 0) {
    ++*neo_indexFinger;
  }
  return tablet_read(*neo_indexFinger, series_long, tablet);
}
uint16_t tablet_retrospective_word_read(const uint16_t indexFinger,
                                        const uint8_t series_long,
                                        const v16us *tablet,
                                        uint16_t *neo_indexFinger) {
  assert(TABLET_LONG * series_long > indexFinger);
  assert(tablet != NULL);
  assert(neo_indexFinger != NULL);
  *neo_indexFinger = indexFinger;
  if (indexFinger % TABLET_LONG == 0) {
    --*neo_indexFinger;
  }
  return tablet_read(*neo_indexFinger, series_long, tablet);
}

void tablet_grammar_write(const uint16_t indexFinger, const uint16_t number,
                          const uint8_t series_long, v16us *tablet,
                          uint16_t *neo_indexFinger) {
  assert(TABLET_LONG * series_long > indexFinger);
  assert(tablet != NULL);
  assert(neo_indexFinger != NULL);
  // printf("%s:%d:%s\n\t", __FILE__, __LINE__, __FUNCTION__);
  // v16us_print(tablet[0]);
  *neo_indexFinger = indexFinger;
  uint16_t list_indexFinger = indexFinger - (indexFinger % TABLET_LONG);
  uint16_t indicator_list = tablet_read(list_indexFinger, series_long, tablet);
  if (indicator_list == 0) {
    indicator_list = 1;
  }
  // printf("%s:%d list_indexFinger 0x%X, indicator_list 0x%04X \n", __FILE__,
  //       __LINE__, list_indexFinger, indicator_list);
  // check if writing beyond end, then skip to next series, and invert previous
  // quote.
  uint16_t binary_phrase_list = 0;
  uint8_t vector_indexFinger = indexFinger % TABLET_LONG;
  uint8_t series_indexFinger = 0;
  uint8_t indicator = 1;
  if (vector_indexFinger == 0) {
    indicator_list = ~indicator_list;
    tablet_write(list_indexFinger, indicator_list, series_long, tablet);
    list_indexFinger = indexFinger;
    printf("%s:%d indexFinger %X\n", __FILE__, __LINE__, indexFinger);
    ++*neo_indexFinger;
    // also invert the previous binary_phrase_list, if it exists.
    series_indexFinger = (uint8_t)indexFinger / TABLET_LONG;
    if (series_indexFinger > 0) {
      binary_phrase_list = (uint16_t)tablet[series_indexFinger - 1].s0;
      indicator = binary_phrase_list & 1;
      printf("%s:%d indicator %X\n", __FILE__, __LINE__, indicator);
      if (indicator == TABLET_FINALLY_INDICATOR) {
        binary_phrase_list = ~binary_phrase_list;
        printf("%s:%d binary_phrase_list %04X\n", __FILE__, __LINE__,
               binary_phrase_list);
        tablet[series_indexFinger - 1].s0 = binary_phrase_list;
        v16us_print(tablet[series_indexFinger - 1]);
      }
    }
    indicator_list = 1;
  }
  // add to indicator list if is case or mood grammar_word
  // assume is mood or grammar word because this was called
  indicator_list |= 1 << (*neo_indexFinger % TABLET_LONG);
  // printf("%s:%d neo_indexFinger 0x%X, indicator_list 0x%04X \n", __FILE__,
  //       __LINE__, *neo_indexFinger, indicator_list);
  tablet_write(list_indexFinger, indicator_list, series_long, tablet);
  // tablet[series_indexFinger].s0 = indicator_list;
  tablet_write(*neo_indexFinger, number, series_long, tablet);
  // printf("%s:%d:%s\n\t", __FILE__, __LINE__, __FUNCTION__);
  //printf("%s:%d\n\t ", __FILE__, __LINE__);
  //v16us_print(tablet[0]);
}
uint16_t tablet_upcoming_grammar_read(const uint16_t indexFinger,
                                      const uint8_t series_long,
                                      const v16us *tablet,
                                      uint16_t *neo_indexFinger) {
  assert(TABLET_LONG * series_long > indexFinger);
  assert(tablet != NULL);
  assert(neo_indexFinger != NULL);
  *neo_indexFinger = indexFinger;
  // get indicator_list for the relevant indexFinger,
  uint16_t indicator_list = 0;
  // get indicator from it (is first bit).
  uint8_t indicator = 0;
  // check for set indicators after indexFinger
  uint8_t list_indexFinger = 0;
  uint8_t series_indexFinger = (uint8_t)(indexFinger / TABLET_LONG);
  uint16_t tablet_indexFinger = 0;
  for (; series_indexFinger < series_long; ++series_indexFinger) {
    indicator_list =
        tablet_read(series_indexFinger * TABLET_LONG, series_long, tablet);
    indicator = indicator_list & 1;
    // printf("%s:%d:%s\n\t 0x%X indicator_list, 0x%X indicator\n", __FILE__,
    // __LINE__,
    //    __FUNCTION__, indicator_list, indicator);
    for (list_indexFinger = 1; list_indexFinger < TABLET_LONG;
         ++list_indexFinger) {
      tablet_indexFinger = series_indexFinger * TABLET_LONG + list_indexFinger;
      if (tablet_indexFinger < indexFinger) {
        list_indexFinger = indexFinger % TABLET_LONG;
      }
      // printf("%s:%d:%s\n\t 0x%X list_indexFinger, 0x%X result\n", __FILE__,
      // __LINE__,
      //    __FUNCTION__, list_indexFinger, (uint16_t) ((indicator_list >>
      //    list_indexFinger) & 1));
      if (((indicator_list >> list_indexFinger) & 1) == indicator) {
        //  if found then return
        *neo_indexFinger = series_indexFinger * TABLET_LONG + list_indexFinger;
        return tablet_read(*neo_indexFinger, series_long, tablet);
      }
    }
    //  if not found then check if indicator is a 0, then can rerun for next in
    //  series.
    if (indicator == 1) {
      break;
    }
    list_indexFinger = 0;
  }
  // if no grammar words found, then return neo_indexFinger 0
  *neo_indexFinger = 0;
  printf("%s:%d:%s ", __FILE__, __LINE__, __FUNCTION__);
  v16us_print(*tablet);
  return 0;
}
uint16_t tablet_upcoming_grammar_or_quote_read(const uint16_t indexFinger,
                                               const uint8_t series_long,
                                               const v16us *tablet,
                                               uint16_t *neo_indexFinger) {
  assert(TABLET_LONG * series_long > indexFinger);
  assert(tablet != NULL);
  assert(neo_indexFinger != NULL);
  *neo_indexFinger = indexFinger;
  uint16_t tablet_indexFinger = 0;
  uint8_t series_indexFinger = (uint8_t)(indexFinger / TABLET_LONG);
  // get indicator_list for the relevant indexFinger,
  uint16_t indicator_list = 0;
  // get indicator from it (is first bit).
  uint8_t indicator = 0;
  // check for set indicators after indexFinger
  uint8_t list_indexFinger = indexFinger % TABLET_LONG;
  uint16_t word_code = 0;
  for (; series_indexFinger < series_long; ++series_indexFinger) {
    //tablet_print(series_long, tablet);
    indicator_list =
        tablet_read(series_indexFinger * TABLET_LONG, series_long, tablet);
    indicator = indicator_list & 1;
    // printf("%s:%d:%s\n\t 0x%X indicator_list, 0x%X indicator, 0x%X
    // series_indexFinger\n", __FILE__, __LINE__,
    //    __FUNCTION__, indicator_list, indicator, series_indexFinger);
    for (list_indexFinger = 1; list_indexFinger < TABLET_LONG;
         ++list_indexFinger) {
      tablet_indexFinger = series_indexFinger * TABLET_LONG + list_indexFinger;
      if (tablet_indexFinger < indexFinger) {
        list_indexFinger = indexFinger % TABLET_LONG;
        tablet_indexFinger =
            series_indexFinger * TABLET_LONG + list_indexFinger;
      }
      // printf("%s:%d:%s\n\t 0x%X list_indexFinger, 0x%X indexFinger, 0x%X
      // result\n", __FILE__, __LINE__,
      //   __FUNCTION__, list_indexFinger, indexFinger, (uint16_t)
      //   ((indicator_list >> list_indexFinger) & 1));
      if (((indicator_list >> list_indexFinger) & 1) == indicator) {
        //  if found then return
        *neo_indexFinger = tablet_indexFinger;
        return tablet_read(tablet_indexFinger, series_long, tablet);
      }
      if (((indicator_list >> (list_indexFinger - 1)) & 1) == indicator) {
        word_code = tablet_upcoming_word_read(tablet_indexFinger, series_long,
                                              tablet, &tablet_indexFinger);
        if ((word_code & (uint16_t)QUOTE_DENOTE_MASK) ==
            (uint16_t)QUOTE_DENOTE) {
          //printf("%s:%d:%s\n\t 0x%X quote_detected\n", __FILE__, __LINE__,
           //      __FUNCTION__, word_code);
          *neo_indexFinger = tablet_indexFinger;
          return word_code;
        }
      }
      //  if not found then check if indicator is a 0, then can rerun for next
      //  in
      //  series.
    }
    if (indicator == 1) {
      break;
    }
    list_indexFinger = 0;
  }
  // if no grammar words found, then return neo_indexFinger at max value
  *neo_indexFinger = 0xFFFF;
  printf("%s:%d:%s\n\t ", __FILE__, __LINE__, __FUNCTION__);
  v16us_print(*tablet);
  return 0;
}

uint16_t tablet_retrospective_grammar_read(const uint16_t indexFinger,
                                           const uint8_t series_long,
                                           const v16us *tablet,
                                           uint16_t *neo_indexFinger) {
  assert(TABLET_LONG * series_long > indexFinger);
  assert(tablet != NULL);
  assert(neo_indexFinger != NULL);
  *neo_indexFinger = indexFinger;
  // get indicator_list for the relevant indexFinger,
  uint16_t indicator_list = 0;
  // get indicator from it (is first bit).
  uint8_t indicator = 0;
  uint8_t iteration = 0;
  // check for set indicators after indexFinger
  uint8_t list_indexFinger = indexFinger % TABLET_LONG;
  uint8_t series_indexFinger = (uint8_t)(indexFinger / TABLET_LONG);
  for (; series_indexFinger < series_long; --series_indexFinger) {
    // printf("%s:%d:%s 0x%X sif\n", __FILE__, __LINE__, __FUNCTION__,
    // series_indexFinger);
    indicator_list =
        tablet_read(series_indexFinger * TABLET_LONG, series_long, tablet);
    indicator = indicator_list & 1;
    //  if not found then check if indicator is a 0, then can rerun for next in
    //  series.
    if (indicator == TABLET_FINALLY_INDICATOR && iteration > 0) {
      break;
    }
    for (; list_indexFinger > 0; --list_indexFinger) {
      // printf("%s:%d:%s 0x%X lif\n", __FILE__, __LINE__,__FUNCTION__,
      // list_indexFinger);
      if (((indicator_list >> list_indexFinger) & 1) == indicator) {
        //  if found then return
        *neo_indexFinger = series_indexFinger * TABLET_LONG + list_indexFinger;
        return tablet_read(*neo_indexFinger, series_long, tablet);
      }
    }
    list_indexFinger = TABLET_LONG - 1;
    ++iteration;
  }
  // if no grammar words found, then return neo_indexFinger 0
  *neo_indexFinger = 0;
  return 0;
}

uint8_t tidbit_read(const uint8_t tidbit_indexFinger, const uint8_t number_long,
                    const uint64_t number) {
  assert(number_long > tidbit_indexFinger);
  return (uint8_t)((number >> tidbit_indexFinger) & 1);
}
void tidbit_write(const uint8_t tidbit_indexFinger, const uint8_t fresh_tidbit,
                  const uint8_t number_long, uint16_t *number) {
  assert(number_long > tidbit_indexFinger);
  assert(fresh_tidbit == 0 || fresh_tidbit == 1);
  if (fresh_tidbit == 1) {
    *number |= 1 << tidbit_indexFinger;
  } else {
    *number &= ~(1 << tidbit_indexFinger);
  }
}

void tablet_upcoming_copy(const uint16_t abl /* source*/, const uint16_t dat /* destination*/,
                          const uint16_t ins /* length*/, const uint8_t series_long,
                          v16us *tablet) {
  assert(dat + ins <= series_long * TABLET_LONG);
  assert(abl < dat);
  assert(tablet != NULL);
  // printf("%s:%d:%s\n\t", __FILE__, __LINE__, __FUNCTION__);
  // v16us_print(*tablet);

  // printf("%s:%d:%s\n\t abl 0x%X dat 0x%X ins 0x%X \n", __FILE__, __LINE__,
  //       __FUNCTION__, abl, dat, ins);
  uint8_t abl_vector_number = 0;
  uint16_t abl_binary_phrase_list = 0;
  // check for edge cases like where destination is a different vector than the
  // source
  uint8_t dat_vector_number = 0;
  uint8_t dat_indicator = 0;
  uint8_t abl_indicator = 0;
  uint8_t dat_tidbit = 0;
  uint8_t abl_tidbit = 0;
  uint16_t dat_binary_phrase_list = 0;
  uint16_t dat_indexFinger = dat + ins;
  uint16_t abl_indexFinger = abl + ins;
  uint16_t abl_word = 0;
  for (; dat_indexFinger >= dat; --dat_indexFinger) {
    abl_word = tablet_retrospective_word_read(abl_indexFinger, series_long,
                                              tablet, &abl_indexFinger);
    tablet_retrospective_word_write(dat_indexFinger, abl_word, series_long,
                                    tablet, &dat_indexFinger);
    // read abl indicator, write dat indicator, delete abl indicator
    abl_vector_number = (uint8_t)abl_indexFinger / TABLET_LONG;
    abl_binary_phrase_list = (uint16_t)tablet[abl_vector_number].s0;
    abl_indicator = abl_binary_phrase_list & 1;
    dat_vector_number = (uint8_t)dat_indexFinger / TABLET_LONG;
    if (dat_vector_number == abl_vector_number) {
      dat_binary_phrase_list = abl_binary_phrase_list;
    } else {
      dat_binary_phrase_list = (uint16_t)tablet[dat_vector_number].s0;
    }
    dat_indicator = dat_binary_phrase_list & 1;
    abl_tidbit = tidbit_read((uint8_t)(dat_indexFinger % TABLET_LONG),
                             CODE_WORD_TIDBIT_LONG, dat_binary_phrase_list);
    if (abl_tidbit == abl_indicator) {
      dat_tidbit = dat_indicator;
    } else {
      dat_tidbit = (~dat_indicator) & 1;
    }
    tidbit_write((uint8_t)(dat_indexFinger % TABLET_LONG), dat_tidbit,
                 CODE_WORD_TIDBIT_LONG, &dat_binary_phrase_list);
    --abl_indexFinger;
    //  printf("%s:%d:%s\n\t ", __FILE__, __LINE__, __FUNCTION__);
    //  v16us_print(*tablet);
  }
}
