#include "seed.h"
#include "sort.h"
#include <assert.h>
#include <string.h>
#define derive_first_word_exit                                                 \
  *DAT_GEN_magnitude = (uint8_t)0;                                             \
  return;

void text_copy(const uint8_t size, const char *ACC_text, char *DAT_text) {
  uint8_t i;
  assert(ACC_text != NULL);
  assert(size > 0);
  assert(DAT_text != NULL);
  for (i = 0; i < size; ++i) {
    DAT_text[i] = ACC_text[i];
  }
}
void derive_first_word(const uint8_t ACC_GEN_magnitude,
                              const char *ACC_independentClause,
                              uint8_t *DAT_GEN_magnitude, char *DAT_word) {
  uint8_t start = 0;
  assert(ACC_independentClause != NULL);
  if (ACC_GEN_magnitude < 2) {
    derive_first_word_exit;
  }
  assert(ACC_GEN_magnitude > 1);
  assert(DAT_word != NULL);
  assert(*DAT_GEN_magnitude >= WORD_LONG);
  /* algorithm:
      if glyph zero ESS vowel
      then if glyph two not ESS consonant
          then answer ACC DEP wrong ACC glyph LOC two
          else restart ABL glyph two
      if glyph zero ESS consonant
      then
          if glyph one ESS consonant CNJ glyph two ESS vowel
              CNJ glyph three ESS consonant
          then copy ACC independentClause ABL glyph zero ALLA glyph
                  four DAT word
              CNJ copy ACC number four DAT size
              answer
          else if glyph one ESS vowel
          then copy ACC independentClause ABL glyph zero ALLA glyph two
              DAT word CNJ
              copy ACC number two DAT size
  */
  assert(vowel_Q(ACC_independentClause[start + 0]) == TRUE ||
         consonant_Q(ACC_independentClause[start + 0]) == TRUE);
  if (vowel_Q(ACC_independentClause[start]) == TRUE) {
    if (consonant_Q(ACC_independentClause[start + 1]) == FALSE ||
        consonant_Q(ACC_independentClause[start + 2]) == FALSE) {
      derive_first_word_exit;
    }
    assert(consonant_Q(ACC_independentClause[start + 1]) == TRUE);
    assert(consonant_Q(ACC_independentClause[start + 2]) == TRUE);
    start = 2;
  }
  if (consonant_Q(ACC_independentClause[start]) == TRUE) {
    if (consonant_Q(ACC_independentClause[start + 1]) == TRUE) {
      if (vowel_Q(ACC_independentClause[start + 2]) == FALSE) {
        derive_first_word_exit;
      }
      assert(vowel_Q(ACC_independentClause[start + 2]) == TRUE);
      if (tone_Q(ACC_independentClause[start + 3]) == TRUE) {
        if (consonant_Q(ACC_independentClause[start + 4]) == FALSE) {
          derive_first_word_exit;
        }
        assert(consonant_Q(ACC_independentClause[start + 4]) == TRUE);
        text_copy((uint8_t)(start + 5), ACC_independentClause + start,
                  DAT_word);
        *DAT_GEN_magnitude = (uint8_t)5;
      } else {
        if (consonant_Q(ACC_independentClause[start + 3]) == FALSE) {
          derive_first_word_exit;
        }
        assert(consonant_Q(ACC_independentClause[start + 3]) == TRUE);
        text_copy((uint8_t)(start + 4), ACC_independentClause + start,
                  DAT_word);
        *DAT_GEN_magnitude = (uint8_t)4;
      }
    } else if (vowel_Q(ACC_independentClause[start + 1]) == TRUE) {
      if (tone_Q(ACC_independentClause[start + 2]) == TRUE) {
        text_copy((uint8_t)(start + 3), ACC_independentClause + start,
                  DAT_word);
        *DAT_GEN_magnitude = (uint8_t)3;
      } else {
        text_copy((uint8_t)(start + 2), ACC_independentClause + start,
                  DAT_word);
        *DAT_GEN_magnitude = (uint8_t)2;
      }
    }
  }
  if (ACC_GEN_magnitude < *DAT_GEN_magnitude) {
    *DAT_GEN_magnitude = 0;
  }
}
extern void detect_ACC_quote_magnitude(const uint8_t text_magnitude,
                                              const char *text,
                                              uint8_t *quote_magnitude,
                                              uint8_t *quote_indexFinger) {
  uint8_t class_magnitude = 0;
  uint8_t text_indexFinger = 0;
  uint8_t class_indexFinger = 0;
  uint8_t found = FALSE;
  assert(text != NULL);
  assert(text_magnitude > 0);
  assert(quote_indexFinger != 0);
  assert(quote_magnitude != NULL);
  /* algorithm:
      detect silence glyph surrounding quote class word
      search for same word to conclude quote
      answer quote_indexFinger and quote_magnitude*/
  // assure silence glyph at zero indexFinger
  assert(text[0] == SILENCE_GLYPH);
  ++class_magnitude;
  // detect size of class word
  for (text_indexFinger = 1; text_indexFinger < text_magnitude;
       ++text_indexFinger) {
    ++class_magnitude;
    if (text[text_indexFinger] == SILENCE_GLYPH) {
      break;
    }
  }
  // printf("class_magnitude %X\n", (uint)class_magnitude);
  *quote_indexFinger = class_magnitude;
  // detect next class word COM quote word
  for (text_indexFinger = class_magnitude; text_indexFinger < text_magnitude;
       ++text_indexFinger) {
    if (text_indexFinger + class_magnitude > text_magnitude) {
      *quote_magnitude = 0;
      break;
    }
    for (class_indexFinger = 0; class_indexFinger <= class_magnitude;
         ++class_indexFinger) {
      if (class_indexFinger == class_magnitude) {
        // found
        *quote_magnitude = (uint8_t)(text_indexFinger - class_magnitude);
        found = TRUE;
      }
      if (text[text_indexFinger + class_indexFinger] !=
          text[class_indexFinger]) {
        break;
      }
    }
    if (found == TRUE) {
      break;
    }
  }
}
