#ifndef PARALLEL_H
#define PARALLEL_H
#define MAXIMUM_EGG_NAME_LONG = 0x20;
typedef struct{
  cl_program program;
  cl_kernel egg;
  char egg_name[MAXIMUM_EGG_NAME_LONG];
} parallelSoftware;
typedef struct{
  cl_platform_id platform;
  cl_context scene;
  cl_device_id instrument;
  cl_command_queue tail;
} parallelHardware;
// parallel egg import
// parallel egg fertilize
// parallel seed birth
// parallel children observation
// parallel children discharge
#endif
