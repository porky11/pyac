Introductory Text\
ryis htet
==================

\_you text read\
sa htet tyac\
\
\
text \_you language teaching\
htet sa pyac hcec\
\
\
\_rea
[realis\_mood](https://en.wikipedia.org/wiki/Grammatical_mood#Realis_moods)
denote\
li hyit dlut\
\
\
\_nom [nominative\_case](https://en.wikipedia.org/wiki/Nominative_case)
denote \_rea\
na hnak dlut li\
\
\
\_acc [accusative\_case](https://en.wikipedia.org/wiki/Accusative_case)
denote \_rea\
ka hkak dlut li\
\
\
\_dat [dative\_case](https://en.wikipedia.org/wiki/Dative_case) denote
\_rea\
yi htik dlut li\
\
\
\_you \_nom phrase grammar text \_acc read \_rea\
sa na fyak hgaf htet ka tyac li\
\
\
text \_nom you \_dat knowledge \_acc giving \_rea\
htetna hnanyi tsinka kwinli\
\
\
phrase grammar \_nom several word phrase \_acc accommodate \_rea\
fyakhgafna kyaktlatfyakka jyakli\
\
\
\_wor \_nom word quoted \_acc denote \_rea\
wona tlatkyitka dlutli\
\
\
\_loc \_wor \_nom
[locative\_case](https://en.wikipedia.org/wiki/Locative_case) \_acc
denote \_rea\
tewona lyokka dlutlia\
\
\
\_you \_nom line two \_loc medium language \_acc see \_rea\
sana lyintyutte hmimpyacka hsatli\
\
\
\_you \_nom line one \_loc natural language word \_and medium language
grammar \_acc see \_rea\
sana lyinhyikte tyicpyactlatwa hmimpyachgafka hsatli\
\
\
\_gen \_wor \_nom
[genitive\_case](https://en.wikipedia.org/wiki/Genitive_case) \_acc
denote \_rea\
tiwona hnikka dlutlia\
\
\
\_you \_nom line three \_loc medium language \_gen phonetic text \_acc
see \_rea\
sana lyintyinte hmimpyacti fyok htet ka hsat li\
/sä.nä ˈljin.tjin.te ˈʰmim.pjäʃ.ti ˈfjok.ʰtet.kä ˈʰsät.li/\
\
\_epi \_wor \_nom
[epistemic\_mood](https://en.wikipedia.org/wiki/Grammatical_mood#Deontic_Mood_vs._Epistemic_Mood)
\_acc denote \_rea\
siwona hsitka dlutli\
\
\
\_you \_nom learn \_acc enjoy \_epi\
sana lyacka hcotsi\
\
\
\_ins \_wor \_nom
[instrumental\_case](https://en.wikipedia.org/wiki/Instrumental_case)
\_acc denote \_rea\
yuwona hyakka dlutli\
/ju.wo.na ˈʰjak.ka ˈdlut.li/\
\
\_you \_nom line one language \_and\_or medium language \_ins computer
program \_acc write \_epi\
sana lyinhyikpyacba hmimpyacyu kyuthromka lyitsi\
\
\
\_abl \_wor \_nom
[ablative\_case](https://en.wikipedia.org/wiki/Ablative_case) \_acc
denote \_rea\
pwihwona hpikka dlutli\
\
\
\_ben \_wor \_nom
[ablative\_case](https://en.wikipedia.org/wiki/Benefactive_case) \_acc
denote \_rea\
pwahwona hpakka dlutli\
\
\
\_plu \_wor \_nom plural\_number \_acc denote \_rea\
puwona hpacka dlutli\
\
computer \_nom natural word \_abl medium word \_dat language \_plu \_ben
translation \_epi\
kyutna tyictlatpwih hmimtlatyi pyacpupwah ryansi\
\
\
\_and medium word \_abl natural word \_dat \_epi\
wa hmimtlatpwih tyictlatyi si\
\
\_top \_wor \_nom topic\_case \_acc denote \_rea\
ta wo na htak ka dlut li\
\
